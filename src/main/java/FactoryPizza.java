/**
 * Created by Ing. Rafael Serate on 23/04/18.
 */
public class FactoryPizza {
    public static IPizza make (String type){
        IPizza pizza;
        switch (type){
            case "carne":
                pizza=new Carne();
                break;
            case "hawaiana":
                pizza=new Hawaiana();
                break;
            default:
                pizza=new Tradicional();
                break;
        }
        return pizza;
    }
}
